<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dolar;
use App\euro;


class MoneyController extends Controller
{
     public function index()
    {
        $dolars = dolar::all();
        $euros = euro::all();

        return view('money')->with(compact('dolars', 'euros'));
    }

    public function store(Request $request)
    {

        //obtenemos json 
        
        $json = '{
    "_antibloqueo": {
        "mobile": "https://dt.wordssl.net",
        "video": "",
        "corto_alternativo": "https://bit.ly/venezuela911",
        "enable_iads": "",
        "enable_admobbanners": "ca-app-pub-8212448379596570/1946229161",
        "enable_admobinterstitials": "",
        "alternativo": "68747470733a2f2f6432663263316f6e64656c3471632e636c6f756466726f6e742e6e6574",
        "alternativo2": "68747470733a2f2f6432663263316f6e64656c3471632e636c6f756466726f6e742e6e6574",
        "notifications": "https://d3c134ru0r3b0g.cloudfront.net",
        "resource_id": "33504 A"
    },
    "_labels": {
        "a": "DOLARTODAY",
        "a1": "CUCUTA (EFECTIVO)",
        "b": "IMPLICITO",
        "c": "DICOM",
        "d": "DOLAR BITCOIN",
        "e": "DIPRO"
    },
    "_timestamp": {
        "epoch": "1565216465",
        "fecha": "Agosto 7, 2019 06:21 PM",
        "fecha_corta": "Ago 7, 2019",
        "fecha_corta2": "Ago 2019",
        "fecha_nice": "Agosto 7, 2019",
        "dia": "Mi�rcoles",
        "dia_corta": "Mi�"
    },
    "USD": {
        "transferencia": 13305.31,
        "transfer_cucuta": 13305.31,
        "efectivo": 45,
        "efectivo_real": 14090.91,
        "efectivo_cucuta": 14090.91,
        "promedio": 13305.31,
        "promedio_real": 12559.49,
        "cencoex": 10.00,
        "sicad1": 12905.44,
        "sicad2": 12559.49,
        "bitcoin_ref": 12905.44,
        "localbitcoin_ref": 12905.44,
        "dolartoday": 13305.31
    },
    "EUR": {
        "transferencia": 14895.96,
        "transfer_cucuta": 14895.96,
        "efectivo": 51.1,
        "efectivo_real": 15786.47,
        "efectivo_cucuta": 15786.47,
        "promedio": 14895.96,
        "promedio_real": 14070.40,
        "cencoex": 11.20,
        "sicad1": 14457.96,
        "sicad2": 14070.40,
        "dolartoday": 14895.96
    },
    "COL": {
        "efectivo": 0.2200,
        "transfer": 0.2200,
        "compra": 0.2200,
        "venta": 0.24
    },
    "GOLD": {
        "rate": 1498.6
    },
    "USDVEF": {
        "rate": 10.2087
    },
    "USDCOL": {
        "setfxsell": 3100.00,
        "setfxbuy": 2725.00,
        "rate": 4118.00,
        "ratecash": 3225.00,
        "ratetrm": 3100.00,
        "trmfactor": 0.2,
        "trmfactorcash": 0.06
    },
    "EURUSD": {
        "rate": 1.12033
    },
    "BCV": {
        "fecha": "1522123200",
        "fecha_nice": "Marzo 27, 2018",
        "liquidez": "328.319.818.517",
        "reservas": "9.444.000"
    },
    "MISC": {
        "petroleo": "57,83",
        "reservas": "9,4"
    }
}';


$data = json_decode($json, true);
      
        //creamos el objeto dolar y asignamos los valores
        $dolars = new dolar();
        $dolars->fecha =  $data['_timestamp']['fecha_corta'];
        $dolars->dolartoday =  $data['USD']['transferencia']; //Valor del Dólar (DolarToday)
        $dolars->dicom =  $data['USD']['sicad1']; //dólar oficial
        $dolars->save(); //guardamos en la bd

        //objeto euro
        $euros = new euro();
        $euros->fecha =  $data['_timestamp']['fecha_corta'];
        $euros->dolartoday =  $data['EUR']['transferencia']; //Valor del Dólar (DolarToday)
        $euros->dicom =  $data['EUR']['sicad1']; //dólar oficial
        $euros->save(); //guardamos en la bd

        
        

    }

}
