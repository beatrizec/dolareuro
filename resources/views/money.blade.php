<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap -->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
        <!-- Styles -->
    
    </head>
    <body>
        <div class="flex-center position-ref full-height">
           <label> Dólar</label>

          <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Dolar - DolarToday </th>
                          <th>Dolar - Dicom</th>
                           
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($dolars as $dolar)
                        <tr>
                          <td>{{ $dolar->fecha }}</td>
                          <td>{{ $dolar->dolartoday }}</td>
                          <td>{{ $dolar->dicom }}</td>
                                                    
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                     <label> Euro</label>
                       <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Euro - DolarToday </th>
                          <th>Euro - Dicom</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($euros as $euro)
                        <tr>
                          <td>{{ $euro->fecha }}</td>
                          <td>{{ $euro->dolartoday }}</td>
                          <td>{{ $euro->dicom }}</td>
                                                    
                        </tr>
                        @endforeach
                      </tbody>
                    </table>

        </div>
    </body>

    <!-- Begin Script de Bootstrap -->
  
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@yield('scripts')
</div>
  <!-- End Script de Bootstrap -->
</html>
